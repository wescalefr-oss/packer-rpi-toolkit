help:
	@echo "USAGE"
	@echo "	make requirements - Install requirements"

requirements:
	pip install --no-cache-dir -U -r requirements.txt
	ansible-playbook playbooks/requirements.yml

build-lcd-raspbian:
	sudo -E packer build packer-builds/lcd-raspbian.json
