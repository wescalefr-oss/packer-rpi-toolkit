# Packer Rpi toolkit

This repository contains our experiments around RaspberryPi provisionning.

## Prerequisistes

* python >= 3.8
* [direnv](https://direnv.net/)

Also based on the [packer-builder-arm-image](https://github.com/solo-io/packer-builder-arm-image) project.
Check carefully their dependencies, they revolve around having a correct setup for:

* [kpartx](https://linux.die.net/man/8/kpartx)
* [qemu-user-static](https://github.com/multiarch/qemu-user-static)
* [binfmt_misc](https://en.wikipedia.org/wiki/Binfmt_misc) kernel capability

## Usage
### Warmup sequence

Run this to get project dependencies.

```
direnv allow
make requirements
```

### Raspbian image build

Needs `sudo` rights.
```
make build-lcd-raspbian
```

## The prototype

We tried a plug-and-almost-play screen shield sold by [Jun Electronic](https://www.amazon.fr/s?me=A5BN6RQOA0WX3&marketplaceID=A13V1IB3VIYZZH).

* [LCD Wiki](http://www.lcdwiki.com/3.5inch_RPi_Display#Driver_Installation)
* [LCD Drivers repository](https://github.com/goodtft/LCD-show)

## Troubleshooting notebook

### The fantabulous misleading error message

This error popped while provisionning the Raspbian image with Ansible.

```
WeScale/pylab/playbooks/raspbian_ansible_chroot.yml:5
    arm-image: fatal: [./raspbian]: UNREACHABLE! => {
    arm-image:     "changed": false,
    arm-image:     "msg": "Authentication or permission failure. In some cases, you may have been able to authenticate and did not have permissions on the target directory. Consider changing the remote tmp path in ansible.cfg to a path rooted in \"/tmp\". Failed command was: ( umask 77 && mkdir -p \"` echo $HOME/.ansible/tmp/ansible-tmp-1580745481.957077-118004487638618 `\" && echo ansible-tmp-1580745481.957077-118004487638618=\"` echo $HOME/.ansible/tmp/ansible-tmp-1580745481.957077-118004487638618 `\" ), exited with result 127",
    arm-image:     "unreachable": true
    arm-image: }
```

The exact issue was that the `mkdir` command was not found because of a wrong `$PATH` propagation to the chroot cage. Worked around this by adding an explicit
`export PATH="${PWD}/.direnv:${PATH}:/bin"` to the project `.envrc`.
